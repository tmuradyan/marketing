<?php

namespace App\Controller\Api;

use App\Services\MarketingApi;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdAccountController extends AbstractFOSRestController {
    /**
     * @Rest\Get("/adaccounts/{adAccount}/limit")
     * @param int $adAccount
     * @return View
     */
    public function getLimit(int $adAccount): View
    {
        $api = new MarketingApi();

        return View::create([
            'limit' => $api->getAccountLimit($adAccount),
        ], Response::HTTP_OK);
    }

    /**
     * @Rest\Patch("/adaccounts/{adAccount}/limit")
     * @param Request $request
     * @param int $adAccount
     * @return View
     */
    public function setLimit(Request $request, int $adAccount): View
    {
        $limit = $request->get('limit');
        $api = new MarketingApi();
        $api->setAccountLimit($adAccount, $limit);

        return View::create([
            'limit' => $limit,
        ], Response::HTTP_OK);
    }
}

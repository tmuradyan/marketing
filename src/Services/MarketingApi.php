<?php

namespace App\Services;

use App\Docker\DockerEnv;
use FacebookAds\Api;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdAccountFields;

/**
 * Class MarketingApi
 * @package App\Services
 */
class MarketingApi
{
    protected $api = null;

    public function __construct()
    {
        if ($this->api === null) {
            $dockerEnv = new DockerEnv();
            $app_id = $dockerEnv->loadSecretFromFile('facebook_app_id');
            $app_secret = $dockerEnv->loadSecretFromFile('facebook_app_secret');
            $access_token = $dockerEnv->loadSecretFromFile('facebook_access_token');

            $this->api = Api::init($app_id, $app_secret, $access_token);
        }
    }

    /**
     * Get Account spend cap and amount spent
     * @param AdAccount $account
     * @return AbstractCrudObject
     */
    public function getAccountFinancialData(AdAccount $account) : AbstractCrudObject
    {
        return $account->read([
            AdAccountFields::SPEND_CAP,
            AdAccountFields::AMOUNT_SPENT,
        ]);
    }

    /**
     * Get Account Limit
     * @param int $adAccount
     * @return integer
     */
    public function getAccountLimit(int $adAccount) : int
    {
        $account = new AdAccount("act_{$adAccount}");
        $financialData = $this->getAccountFinancialData($account);

        return (int) $financialData->spend_cap - (int) $financialData->amount_spent;
    }

    /**
     * Set Account Limit
     * @param int $adAccount
     * @param int $limit
     * @return AbstractCrudObject
     */
    public function setAccountLimit(int $adAccount, int $limit) : AbstractCrudObject
    {
        $account = new AdAccount("act_{$adAccount}");
        $financialData = $this->getAccountFinancialData($account);

        return $account->update([
            AdAccountFields::SPEND_CAP => (int) $financialData->amount_spent + (int) $limit,
        ]);
    }
}
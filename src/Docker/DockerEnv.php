<?php

namespace App\Docker;

/**
 * Class DockerEnv
 * Used to work with Docker environment
 * @package App\Docker
 */
class DockerEnv
{
    /**
     * Load secrets from file
     * @param string $filename
     * @return string
     */
    public function loadSecretFromFile(string $filename) : string
    {
        return file_get_contents("/run/secrets/{$filename}");
    }
}
